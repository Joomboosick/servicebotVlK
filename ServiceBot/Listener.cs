﻿using Microsoft.Extensions.Logging;
using NLog;
using ServiceBotApi;
using ServiceBotApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VkNet;
using VkNet.Enums.SafetyEnums;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace ServiceBot
{
    class Listener
    {
        Logger logger;
        private Dictionary<string, object> settings = new Dictionary<string, object>();
        KeyboarCreater KeyboardCreater = new KeyboarCreater(new VkNet.Model.Keyboard.KeyboardBuilder("text"));
        StateService stateService = new StateService();
        StateProcessor stateProcessor;
        private ServiceBotAPI _apiClient;
        MessageWrapper msg;
        private const int MessengerType = 3;
        public Listener(Dictionary<string, object> settings)
        {
            logger = LogManager.GetCurrentClassLogger();
            this.settings = settings;
        }
        VkApi api;
        LongPollServerResponse server;
        string Ts;
        string groupId;
        bool flag = true;
        users botSettings;

        public void listen()
        {
            api = (VkApi)this.settings["api"];
            _apiClient = (ServiceBotAPI)this.settings["apiClient"];
            server = (LongPollServerResponse)this.settings["server"];
            Ts = server.Ts;
            groupId = this.settings["groupid"].ToString();
            botSettings = (users)settings["botSettings"];
            int num = 0;
            // устанавливаем метод обратного вызова
            TimerCallback tm = new TimerCallback(CheckUpdates);
            // создаем таймер
            Timer timer = new Timer(tm, num, 0, 500);
            while (true) Thread.Sleep(10) ;


        }
        void CheckUpdates(object obj)
        {
            if (flag)
            {
                flag = false;
                BotsLongPollHistoryResponse poll;
                try
                {
                    poll = api.Groups.GetBotsLongPollHistory(
                       new BotsLongPollHistoryParams()
                       { Server = server.Server, Ts = Ts.ToString(), Key = server.Key, Wait = 1 });
                }
                catch (VkNet.Exception.LongPollKeyExpiredException e)
                {
                    var settings = BotSettings.getSettings();
                    server = api.Groups.GetLongPollServer(ulong.Parse(groupId));
                    this.settings["server"] = server;
                    poll = api.Groups.GetBotsLongPollHistory(
                       new BotsLongPollHistoryParams()
                       { Server = server.Server, Ts = Ts.ToString(), Key = server.Key, Wait = 1 });
                    logger.Info(e, "update longpoll server");
                }
                if (poll?.Updates == null) return;
                foreach (var a in poll.Updates)
                {

                    if (a.Type == GroupUpdateType.MessageNew)
                    {
                        var messageNew = a.MessageNew;
                        var message = a.Message;
                        //doing something to proccess new message
                        var user = api.Users.Get(new long[] { (long)a.Message.UserId }).FirstOrDefault();

                        var curentState = getCurrentState(groupId, user).Result;
                        var state = stateService.GetNextState(message.Body, curentState);

                        if (state == null || state == "")
                        {
                            msg = new MessageWrapper(api, groupId);
                            var payload = msg.GetPayload(message.Id).text;
                            if (payload != null && payload != "fksdf" && payload != "text1231312")
                                state = payload.Split('@').FirstOrDefault();
                            else state = "startBooking";
                        }

                        stateProcessor = new StateProcessor(user, message, state, groupId, _apiClient, api,botSettings);
                        logger.Info(user.FirstName + " " + user.LastName + ": " + a.Message.Body);

                        Ts = (Int32.Parse(Ts) + 1).ToString(); //after each response we have to increase this string value
                    }
                }
                flag = true;
            }
        }



        async Task<string> getCurrentState(string groupId, VkNet.Model.User user)
        {

            try
            {
                var history = await _apiClient.GetHistoryAsync(messenger: MessengerType, messengerBotId: groupId, clientMessengerId: user.Id.ToString());
                MessengerHistoryDto messengerHistory = new MessengerHistoryDto();
                if (history.MessengerHistory.Count > 0)
                {
                    messengerHistory = history.MessengerHistory.Last<MessengerHistoryDto>();

                    if (messengerHistory.StateMetaData.Length > 0)
                        return messengerHistory.StateMetaData;
                }
            }
            catch (Exception e)
            {
                logger.Warn("cant get history");
            }



            return "";
        }


    }
}
