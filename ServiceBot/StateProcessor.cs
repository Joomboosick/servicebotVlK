﻿using Newtonsoft.Json;
using ServiceBot.models;
using ServiceBotApi;
using ServiceBotApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VkNet;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace ServiceBot
{
    class StateProcessor
    {
        User user;
        Message message;
        MessageWrapper msg;
        string state;
        string groupId;

        const string ST_menu = "menu";
        const string ST_startBooking = "startBooking";
        const string ST_cancelBooking = "cancelBooking";
        const string ST_pricelist = "pricelist";
        const string ST_changeBranch = "changeBranch";
        const string ST_prevStep = "prevStep";
        const string ST_companySelect = "companySelect";

        const string startBookingBTN = "Записаться";

        private const int MessengerType = 3;
        private const string SelectServiceText = "Вы выбрали:";
        private const string SelectCityText = "Выберите город:";
        private const string SelectCompanyText = "Выберите компанию:";
        private const string ChangeCompanyText = "Сменить филиал";
        private const string ResetCityText = "Укажите город:";
        private const string ResetCompanyText = "Укажите компанию:";
        private const string SelectScenarioText = "Выберите сценарий:";
        private const string SelectStaffText = "Barber:";
        private const string SelectCategoryText = "Услуги";
        private const string SelectDateText = "Выберите, пожалуйста, дату:";
        private const string PriceText = "Прайс-лист";
        private const string AboutText = "О нас";
        private const string IncorrectDataText = "Введены неправильные данные, повторите попытку";
        private const string confirmSelectCategoryText = "вперед ➡️";
        private const string confirmSelectServiceText = "вперед ➡️";

        private const char selectItem = '✅';
        private const char unselectItem = '☑';

        private ServiceBotAPI _apiClient;

        static VkNet.Model.Keyboard.KeyboardBuilder keyboard = new VkNet.Model.Keyboard.KeyboardBuilder("text");
        KeyboarCreater KeyboarCreater = new KeyboarCreater(keyboard);

        NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        users botSettings;
        VkApi _api;

        private IDictionary<string, int> ScenarioByName = new Dictionary<string, int>
        {
            ["Мастер"] = 1,
            ["Услуга"] = 2,
            ["Время"] = 3
        }; 
        private struct mId
        {
            public IEnumerable<ulong> MessageId;
            public IEnumerable<string> fields;
        }
        public StateProcessor(User user, Message message, string state, string groupId, ServiceBotAPI _apiclient, VkApi api, users settings)
        {
            this.user = user;
            this.message = message;
            this.state = state;
            this.groupId = groupId;
            _apiClient = _apiclient;
            _api = api;
            botSettings = settings;
            msg = new MessageWrapper(_api,this.groupId);
            doWork();
            
        }

        private void doWork()
        {
            logger.Info("state: "+this.state);
            if(this.state.Contains(ST_startBooking))
            {
                startBooking();
                return;
            }
            switch (this.state)
            {
                case "call":
                    getCallText(); 
                    break;
                case ST_startBooking:
                    startBooking();
                    return;
                    break;
                case ST_menu:
                    getMenu();
                    return;
                    break;
                case ST_pricelist:
                    getPricelist();
                    return;
                    break;
                case ST_companySelect:
                    CompanySelect();
                    return;
                    break;
                case ST_prevStep:
                    getPrevStep();
                    return;
                    break;
                case ST_cancelBooking:
                    chancelBooking();
                    break;
                case ST_changeBranch:
                    changeBranch();
                    return;
                    break;
                default:
                    changeBranch();
                    break;
            }
        }

        private async Task getCallText()
        {
            var companyInfo = await _apiClient.GetClientPreferredCompanyInfoAsync(messenger: MessengerType, messengerBotId: groupId, clientMessengerId: user.Id.ToString());
            msg.sendMessage(user.Id, $"Наш номер: {companyInfo.Phone}");
        }

        private async Task getMenu()
        {
            var companyInfo = await _apiClient.GetClientPreferredCompanyInfoAsync(messenger: MessengerType, messengerBotId: groupId, clientMessengerId: user.Id.ToString());
            if (companyInfo.IsChangeCompanyLock.HasValue && companyInfo.IsChangeCompanyLock.Value)
            {
                msg.sendMessage(user.Id, "Чтобы записаться нажмите записаться", KeyboarCreater.createStartKeyboard(false));
            }
            else
            {
                msg.sendMessage(user.Id, "Чтобы записаться нажмите записаться", KeyboarCreater.createStartKeyboard());
            }

        }

        private async Task chancelBooking()
        {
            var _payload = await GetPayload(message.Id);
            if (_payload != null && _payload.text.Split('@')[0] == ST_cancelBooking) 
            {
                string visitMsgId = _payload.text.Split('@').Last();
                await _apiClient.DeleteBookingAsync(visitMessengerMsgId: visitMsgId, messenger: MessengerType, messengerBotId: groupId, clientMessengerId: user.Id.ToString());
                msg.sendMessage(user.Id, "Заявка успешно отменена",logging: false);
            }
            var bookings = await _apiClient.GetPlannedBookingsAsync(messenger: MessengerType, clientMessengerId: user.Id.ToString(), messengerBotId: groupId);
            if(!bookings.Items.Any())
            {
                var keyb = KeyboarCreater.createStartKeyboard();
                msg.sendMessage(user.Id, "У вас пока нет записей. Вы можете записаться, нажав на кнопку 'Записаться'", keyb);
            }
            else
            {
                
                var dict = new Dictionary<string, long?>();
                var items = new List<string>();
                var payload = new List<string>();
                var ids = new SetMessengerMsgIdDto
                {
                    ClientMessengerId = user.Id.ToString(),
                    Messenger = MessengerType,
                    MessengerBotId = groupId,
                };
                for (int i = 1; i < bookings.Items.Count+ 1; i++)
                {
                    var describ = bookings.Items[i - 1].CancelMessage.Replace("Ваша действующая заявка!", "");
                    describ = describ.Replace("\r\n", " ");
                    describ = describ.Remove(describ.IndexOf("Мастер"), describ.Length - describ.IndexOf("Мастер"));
                    items.Add(describ);
                    payload.Add($"{ST_cancelBooking}@{message.Id}_{i}");
                    dict.Add($"{message.Id}_{i}", bookings.Items[i - 1].VisitId);
                    msg.sendMessage(user.Id, bookings.Items[i - 1].CancelMessage);
                }
                var keyb = KeyboarCreater.createKeyboardWithPayloadForEveryButton(items.ToArray(), payload.ToArray(), 1,"Вернуться в меню");
                ids.MessageIdVisitIdDict = dict;
                msg.sendMessage(user.Id, "Выберите нужный визит для удаления", keyb);
                await _apiClient.SetMessengerMsgIdsInVisitsAsync(ids);
            }
        }


        private async Task changeBranch()
        {

            if (message.Body == "Начать")
            {
                var startResult = await _apiClient.GetStartAsync(messenger: MessengerType, clientMessengerId: user.Id.ToString(), messengerBotId: groupId);
                if (startResult.IsCitySelection == null || startResult.IsCitySelection == false)
                {
                    string hello = startResult.HelloMessage == string.Empty ? string.Empty : startResult.HelloMessage;
                    if (hello != string.Empty)
                        msg.sendMessage(user.Id, hello);
                    startBooking(true);
                    return;
                }
                if (startResult.IsCitySelection == true)
                {
                    var response = await _apiClient.ResetPreferredCompanyGetCitiesAsync(new BookingInputDto(messenger: MessengerType, messengerBotId: groupId, clientMessengerId: user.Id.ToString()));
                    List<string> _payload = new List<string>();
                    foreach (var city in response.CityList)
                        _payload.Add(ST_changeBranch + "@" + city);
                    var keyb = KeyboarCreater.createKeyboardWithPayloadForEveryButton(response.CityList.ToArray<string>(), _payload.ToArray(), 3);
                    msg.sendMessage(user.Id, ResetCityText, keyb);
                    return;
                }
                if (startResult.CityList == null || startResult.CityList.Count <= 1)
                {
                    string hello = startResult.HelloMessage == string.Empty ? string.Empty : startResult.HelloMessage;
                    if (hello != string.Empty)
                        msg.sendMessage(user.Id, hello);
                    startBooking(true);
                    return;
                }

                msg.sendMessage(user.Id, "Не удалось начать, напишите в чат \"меню\"");
                return;
            }
            else
            {
                var payload = await GetPayload(message.Id);

                if (payload.text == "text1231312")
                {
                    var response = await _apiClient.ResetPreferredCompanyGetCitiesAsync(new BookingInputDto(messenger: MessengerType, messengerBotId: groupId, clientMessengerId: user.Id.ToString()));
                    List<string> _payload = new List<string>();
                    foreach (var city in response.CityList)
                        _payload.Add(ST_changeBranch + "@" + city);
                    var keyb = KeyboarCreater.createKeyboardWithPayloadForEveryButton(response.CityList.ToArray<string>(), _payload.ToArray(), 3);
                    msg.sendMessage(user.Id, ResetCityText, keyb);
                }
                else
                {
                    try
                    {
                        var response = await _apiClient.ResetPreferredCompanyGetCompaniesAsync(new ResetCompaniesGetDto(selectedCityName: payload.text.Split('@')[1], messenger: MessengerType, messengerBotId: groupId, clientMessengerId: user.Id.ToString()));


                        if (response.CompanyList.Contains(message.Body))
                        {
                            await _apiClient.ResetPreferredCompanySetCompanyAsync(new ResetCompanyDto(message.Body, MessengerType, groupId, user.Id.ToString()));
                            startBooking(true);
                            return;
                        }
                        else
                        {

                            int placeholder;
                            int.TryParse(this.GetPayload(message.Id).Result.text.Split('@').Last(), out placeholder);
                            if (placeholder == null) placeholder = 0;
                            var keyb = KeyboarCreater.createCompaniesKeyboard(response.CompanyList.ToArray<string>(), placeholder, state, payload.text.Split('@')[1]);
                            msg.sendMessage(user.Id, ResetCompanyText, keyb);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Warn("incorrect data from user ");
                        msg.sendMessage(user.Id, IncorrectDataText, KeyboarCreater.createKeyboardMenu());
                        throw;
                    }
                }
            }

        }
        string[] vs = new string[] { "40", "60", "80", "70", "100", "110", "130", "90" };
        private async Task getPrevStep()
        {
            var scenarioInfo = await _apiClient.GetCurrentScenarioAsync(
                messenger: MessengerType,
                messengerBotId: groupId,
                clientMessengerId: user.Id.ToString()
                );
            var h = await _apiClient.GetHistoryAsync(3, groupId, user.Id.ToString());
            if (scenarioInfo.ScenarioCurrentStep != h.MessengerHistory.Last().Step && vs.Contains(h.MessengerHistory.Last().Step.ToString()))
                    scenarioInfo.ScenarioCurrentStep = h.MessengerHistory.Last().Step;
            state = ST_startBooking;
            //100 date
            int? step = null;
            switch (scenarioInfo.CurrentScenario)
            {
                case 1:
                    if (scenarioInfo.ScenarioCurrentStep == 40) 
                        step = (int?)VisitCreationStep.SetCompany;
                    if(scenarioInfo.ScenarioCurrentStep == 60)
                    {
                        step = (int?)VisitCreationStep.SetScenario;
                        var msg = GetMessage(VisitCreationStep.GetStaff);
                        message.Body = msg.Text;
                    }

                    if (scenarioInfo.ScenarioCurrentStep == 70)
                    {
                        step = (int?)VisitCreationStep.SetScenario;
                        var msg = GetMessage(VisitCreationStep.GetStaff);
                        message.Body = msg.Text;
                    }
                    if (scenarioInfo.ScenarioCurrentStep == 80)
                    {
                        step = (int?)VisitCreationStep.GetCategories;
                        var msg = GetMessage(VisitCreationStep.SetServices);
                        message.Body = " ";
                        message.Payload = msg.Payload;
                    }

                    if (scenarioInfo.ScenarioCurrentStep == 90)
                    {
                        step = (int?)VisitCreationStep.SetServices;
                        var curMsg = await GetPayload(message.Id);
                        Message msg = null;
                        if (curMsg.text == "0" || curMsg.text == "cat" || curMsg.text == "fksdf")
                        {
                            step = (int?)VisitCreationStep.GetCategories;
                            msg = GetMessage(VisitCreationStep.SetServices);
                        }
                        else
                        {
                            var catIndex = int.Parse(curMsg.text) - 1;
                            
                            msg = GetMessage(VisitCreationStep.SetServices, last: true, contains: catIndex.ToString());
                            state = getDefinedHistory(VisitCreationStep.SetServices,lastHistory: true,messageId: msg.Id.ToString()+"_prev").Result.StateMetaData;
                        }
                        message.Body = " ";
                        message.Payload = msg.Payload;
                    }

                    if (scenarioInfo.ScenarioCurrentStep == 100)
                    {
                        step = (int?)VisitCreationStep.SetServices;
                        var msg = GetMessage(VisitCreationStep.GetDates);
                        var payload = await GetPayload(message.Id);
                        if (payload.text != "date")
                        {
                            message.Body = " ";
                            
                        }
                        else
                        {
                            message.Body = msg.Text;
                            
                        }
                        state = getDefinedHistory(VisitCreationStep.SetServices, true).Result.StateMetaData;
                        message.Payload = msg.Payload;

                    }
                    if (scenarioInfo.ScenarioCurrentStep == 120)
                    {
                        step = (int?)VisitCreationStep.SetServices;
                        state = getDefinedHistory(VisitCreationStep.SetServices, true).Result.StateMetaData;
                        var msg = GetMessage(VisitCreationStep.GetDates);
                        message.Body = msg.Text;
                        message.Payload = msg.Payload;
                    }

                    if (scenarioInfo.ScenarioCurrentStep == 130)
                    {
                        step = (int?)VisitCreationStep.GetDates;
                        var msg = GetMessage(VisitCreationStep.GetDates,true);
                        message.Body = msg.Text;
                        message.Payload = msg.Payload;
                        message.Id = msg.Id;
                    }

                    break;
                case 2:
                    if (scenarioInfo.ScenarioCurrentStep == 40)
                        step = (int?)VisitCreationStep.SetCompany;

                    if (scenarioInfo.ScenarioCurrentStep == 80)
                    {
                        step = (int?)VisitCreationStep.GetCategories;
                        var msg = GetMessage(VisitCreationStep.SetServices);
                        message.Body = " ";
                        message.Payload = msg.Payload;
                    }

                    if (scenarioInfo.ScenarioCurrentStep == 50)
                    {
                        step = (int?)VisitCreationStep.SetServices;
                        state = getDefinedHistory(VisitCreationStep.SetServices, true).Result.StateMetaData;
                        var msg = GetMessage(VisitCreationStep.GetStaff);
                        message.Body = " ";
                        message.Payload = msg.Payload;
                    }
                    if (scenarioInfo.ScenarioCurrentStep == 60)
                    {
                        step = (int?)VisitCreationStep.SetServices;
                        state = getDefinedHistory(VisitCreationStep.SetServices, true).Result.StateMetaData;
                        var msg = GetMessage(VisitCreationStep.GetDates);
                        message.Body = msg.Text;
                        message.Payload = msg.Payload;
                    }

                    if (scenarioInfo.ScenarioCurrentStep == 110)
                    {
                        step = (int?)VisitCreationStep.GetStaff;
                        var msg = GetMessage(VisitCreationStep.GetDates);
                        message.Body = msg.Text;
                        message.Payload = msg.Payload;
                    }
                    break;
                case 3:
                    if (scenarioInfo.ScenarioCurrentStep == 40)
                        step = (int?)VisitCreationStep.SetCompany;

                    if (scenarioInfo.ScenarioCurrentStep == 70) 
                    {
                        step = (int?)VisitCreationStep.SetScenario;
                        var msg = GetMessage(VisitCreationStep.GetDates);
                        message.Body = msg.Text;
                        message.Payload = msg.Payload;
                        await _apiClient.DeleteAllHistoryAsync(messenger: MessengerType, groupId, user.Id.ToString());
                    }

                    if (scenarioInfo.ScenarioCurrentStep == 80)
                    {
                        step = (int?)VisitCreationStep.GetCategories;
                        var msg = GetMessage(VisitCreationStep.SetServices);
                        message.Body = " ";
                        message.Payload = msg.Payload;
                    }
                    if (scenarioInfo.ScenarioCurrentStep == 50)
                    {
                        step = (int?)VisitCreationStep.SetServices;
                        state = getDefinedHistory(VisitCreationStep.SetServices, true).Result.StateMetaData;
                        var msg = GetMessage(VisitCreationStep.GetStaff);
                        message.Body = " ";
                        message.Payload = msg.Payload;
                    }

                    break;
                default:
                    state = ST_menu;
                    doWork();
                    return;
                    break;
            }
            await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
                visitStep: step,
                messengerMsgId: message.Id.ToString(),
                stateMetaData: state,
                messenger: MessengerType,
                messengerBotId: groupId,
                clientMessengerId: user.Id.ToString()
                ));
            doWork();
        }

        private async Task startBooking(bool first = false)
        {
            BookingStartResultDto startInfo = new BookingStartResultDto();
            
            try
            {
                int? step = null;
                try
                {

                    if (message.Body == startBookingBTN || first)
                    {
                       
                        try
                        {

                            await _apiClient.DeleteAllHistoryAsync(
                                messenger: MessengerType,
                                messengerBotId: groupId,
                                clientMessengerId: user.Id.ToString()
                                );

                            
                        }
                        catch (Exception e)
                        {
                            logger.Warn(e.Message + "can't delete history");
                            startInfo = await _apiClient.GetStartAsync(
                            saveHistory: false,
                            messengerMsgId: message.Id.ToString(),
                            stateMetaData: ST_startBooking,
                            messenger: MessengerType,
                            messengerBotId: groupId,
                            clientMessengerId: user.Id.ToString()
                            );
                            first = false;
                        }
                        if (first)
                            startInfo = await _apiClient.GetStartAsync(
                            saveHistory: false,
                            messengerMsgId: message.Id.ToString(),
                            stateMetaData: ST_startBooking,
                            messenger: MessengerType,
                            messengerBotId: groupId,
                            clientMessengerId: user.Id.ToString()
                            );
                    }
                    var history = await _apiClient.GetHistoryAsync(
                                messenger: MessengerType,
                                messengerBotId: groupId,
                                clientMessengerId: user.Id.ToString()
                                );
                    step = history.MessengerHistory.LastOrDefault()?.Step;

                }
                catch (Exception e)
                {
                    logger.Error(e, "cant get history");
                    logger.Trace($"groupid: {groupId}\n clientID: {user.Id}");
                    throw;
                }
                
                if ((step == null || step < 41) && step != 0)
                {
                    startInfo = await _apiClient.GetStartAsync(
                    saveHistory: false,
                    messengerMsgId: message.Id.ToString(),
                    stateMetaData: ST_startBooking,
                    messenger: MessengerType,
                    messengerBotId: groupId,
                    clientMessengerId: user.Id.ToString()
                    );
                }
                if (startInfo.IsCitySelection == true)
                {
                    changeBranch();
                    return;
                }
                if (message.Body.Contains("повторить"))
                {
                    step = (int?)VisitCreationStep.GetDates;
                    var msg = GetMessage(VisitCreationStep.Finalize);
                    var msg1 = GetMessage(VisitCreationStep.GetDates, true);
                    message.Body = msg.Text;
                    message.Payload = msg1.Payload;
                    message.Id = msg.Id;
                }
               switch (step)
                {
                    case (int?)VisitCreationStep.SetCompany:
                        getScenarioBooking(startInfo);
                        break;
                    case (int?)VisitCreationStep.SetScenario:
                        setScenarioBooking(startInfo);
                        break;
                    case (int?)VisitCreationStep.GetStaff:
                        setStaffBooking();
                        break;
                    case (int?)VisitCreationStep.GetCategories:
                        getSelectedCategory();
                        break;
                    case (int?)VisitCreationStep.SetServices:
                        getSelectedServices();
                        break;
                    case (int?)VisitCreationStep.GetDates:
                        GetTime();
                        break;
                    case (int?)VisitCreationStep.GetTimes:
                        SetTimes();
                        break;
                    case (int?)VisitCreationStep.Undefined:
                        setPhone();
                        break;
                        
                    case (int?)VisitCreationStep.Finalize:
                        SmsOrFinalize();
                        break;
                    default:
                        getScenarioBooking(startInfo);
                        break;
                }
            }
            catch(Exception e )
            {
                logger.Error(e, "unexpected error, get state");
                throw;
            }
        }

        private async Task SmsOrFinalize(payload _payload = null)
        {
            BookingSetTimeResultDto setPhoneResult = null;
            bool IsDigit = false;
                try
                {
                    IsDigit = message.Body.Length == message.Body.Where(c => char.IsDigit(c)).Count();
                    if (IsDigit)
                    {
                        var response = await _apiClient.FinalizeBookingAsync(new BookingFinalizeDto(
                            smsCode: message.Body,
                            saveHistory: false,
                            messengerMsgId: message.Id.ToString(),
                            stateMetaData: state,
                            messenger: MessengerType,
                            messengerBotId: groupId,
                            clientMessengerId: user.Id.ToString()
                            ));
                        var keyb = KeyboarCreater.createStartKeyboard();
                        msg.sendMessage(user.Id, response.FinishMessage, keyb);
                    }
                    else
                    {
                        try
                        {

                            var response = await _apiClient.FinalizeBookingAsync(new BookingFinalizeDto(
                                saveHistory: false,
                                messengerMsgId: message.Id.ToString(),
                                stateMetaData: state,
                                messenger: MessengerType,
                                messengerBotId: groupId,
                                clientMessengerId: user.Id.ToString()
                                ));
                            var keyb = KeyboarCreater.createStartKeyboard();
                            msg.sendMessage(user.Id, response.FinishMessage, keyb);
                        }
                        catch (Exception e)
                        {
                        logger.Error(e, "cant finalize booking");
                        logger.Trace($"message id: {message.Id} \ngroupId:{groupId} \n clientId: {user.Id}");
                            msg.sendMessage(user.Id, IncorrectDataText);
                        throw;
                    }
                        
                    }
                }
                catch (Exception e)
                 {
                logger.Error(e, "cant finalize booking");
                logger.Trace($"message body: {message.Body} \ngroupId:{groupId} \n clientId: {user.Id} \n isDigit: {IsDigit}");
                throw;
            }
            }
            

        private Message GetMessage(VisitCreationStep step, bool last = false, int? selection = null, string contains = "")
        {
            int k = 0;
            var history = _apiClient.GetHistory(MessengerType, groupId, user.Id.ToString());
            Message msg = null;
            Message msgtoout = null;
            for (int i = 0; i < history.MessengerHistory.Count; i++)
            {
                MessengerHistoryDto h = history.MessengerHistory[i];
                if (h.Step != (int?)step)
                    continue;

                if (h.Step == (int?)step)
                {

                    if (selection != null && k < selection)
                    {
                        k++;
                        continue;
                    }
                    
                    mId id;
                    List<ulong> MessageList = null;
                    try
                    {
                        MessageList = new List<ulong> { ulong.Parse(h.MessageId) };
                    }
                    catch (Exception e)
                    {
                        MessageList = new List<ulong> { ulong.Parse(h.MessageId.Replace("_prev","")) };
                    }
                    
                    List<string> FieldList = new List<string> { "" };
                    id.MessageId = MessageList;
                    id.fields = FieldList;
                    msg = _api.Messages.GetById(id.MessageId, id.fields, 0, false, ulong.Parse(groupId))[0];

                        var p = JsonConvert.DeserializeObject<ServiceBot.models.payload>(msg.Payload);
                    if (!p.text.Contains(contains))
                        continue;
                    else if (p.text.Contains(contains))
                        msgtoout = msg;
                    else if(p.text.Split('@').Length > 1)
                    {
                        var wsP = p.text.Split('@');
                            if (wsP[1] != "➡️" || wsP[1] != "⬅️")
                                return msg;
                        
                    }
                    
                    if (contains == ""&&!last && selection == null)
                    {
                        msg = _api.Messages.GetById(id.MessageId, id.fields, 0, false, ulong.Parse(groupId))[0];

                        return msg;
                    }
                    
                }
               
            }
            if (contains == "")
                return msg;
            else return msgtoout;
        }

        private async Task setPhone()
        {
            var msg = GetMessage(VisitCreationStep.Undefined, last: true);
            await _apiClient.SetPhoneAsync(new SetPhoneInputDto(message.Body, MessengerType, groupId, user.Id.ToString()));

            var p = JsonConvert.DeserializeObject<ServiceBot.models.payload>(msg.Payload);
            p.text = msg.Text;
            await SetTimes(p);
        }

        private async Task SetTimes(payload _payload = null, bool separate = false, bool setTime3rd = true)
        {
            var scenario = _apiClient.GetCurrentScenarioAsync(MessengerType, groupId, user.Id.ToString()).Result.CurrentScenario;
            if (separate)
            {
                _payload.text = _payload.text.Split('@')[1];
            }
            if(_payload == null)
                _payload = await GetPayload(message.Id);
            BookingSetTimeResultDto setPhoneResult = null;
            bool istime = _payload.text.Contains(':');
            if (scenario == 1 || scenario == 2 || istime)
            {
                try
                {
                    if(istime)
                        setPhoneResult = (await _apiClient.SetScheduleTimeAsync(new BookingSetScheduleTimeDto(
                        selectedTime: _payload.text,
                        saveHistory: true,
                        messengerMsgId: message.Id.ToString(),
                        stateMetaData: state,
                        messenger: MessengerType,
                        messengerBotId: groupId,
                        clientMessengerId: user.Id.ToString()
                        )));
                    
                }
                catch (Exception)
                {
                    var history = await getDefinedHistory(VisitCreationStep.Undefined);
                    _payload = await GetPayload(long.Parse(history.MessageId));
                }
                var hasPhone = setPhoneResult.HasClientPhone.Value;
                if (!hasPhone)
                {


                    await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(visitStep: (int?)VisitCreationStep.Undefined,
                        messengerMsgId: message.Id.ToString(),
                        stateMetaData: state,
                        messenger: MessengerType,
                        messengerBotId: groupId,
                        clientMessengerId: user.Id.ToString()
                        ));

                    msg.sendMessage(user.Id, "Введите Ваш номер в формате 89031234356");
                    return;
                }
                else
                {
                    if (setPhoneResult.IsSmsConfirmationRequested == true)
                    {

                        var keyb = KeyboarCreater.createKeyboardWithBackBTN(new string[] { "повторить" });
                        msg.sendMessage(user.Id, "На Ваш телефон будет отправлено смс-сообщение с кодом подтверждения. Введите его. " +
                                                 "Если смс не пришло, нажмите кнопку повторить, и сообщение будет отправлено Вам повторно.", keyb);
                    }
                    else
                        SmsOrFinalize();
                    await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(visitStep: (int?)VisitCreationStep.Finalize,
                        messengerMsgId: message.Id.ToString(),
                        stateMetaData: state,
                        messenger: MessengerType,
                        messengerBotId: groupId,
                        clientMessengerId: user.Id.ToString()
                        ));
                }
            }
            if(scenario == 3 && !istime)
            {
                    var setScenarioResult = await _apiClient.SetScenarioAsync(new BookingSetScenarioDto(scenario,
                        saveHistory: false,
                        messengerMsgId: message.Id.ToString(),
                        stateMetaData: state,
                        messenger: MessengerType,
                        messengerBotId: groupId,
                        clientMessengerId: user.Id.ToString()
                        ));
                    getCategoryList(setScenarioResultResponse: setScenarioResult, _sendMessege: true, scenario: 3);
                    await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
                        visitStep: (int?)VisitCreationStep.GetCategories,
                        messengerMsgId: message.Id.ToString(),
                        stateMetaData: state,
                        messenger: MessengerType,
                        messengerBotId: groupId,
                        clientMessengerId: user.Id.ToString()
                        ));
                
            }

        }

        models.payload tmp;

        private async Task GetTime()
        {
            BookingScheduleTimesResultDto times = new BookingScheduleTimesResultDto();
            BookingSetStaffResultDto setStaffResponse = new BookingSetStaffResultDto();
            var scenario = _apiClient.GetCurrentScenario(MessengerType, groupId, user.Id.ToString()).CurrentScenario;
            var payload = msg.GetPayload(message.Id);
            if(scenario == 3)
            {
                var _msg = GetMessage(VisitCreationStep.GetStaff,selection: 1);
                if (_msg != null)
                {
                    try
                    {
                        if (_msg.Text != "Подтвердить выбранные сервисы" && !_msg.Text.Contains('.'))
                            setStaffResponse = await _apiClient.SetStaffAsync(new BookingSetStaffDto(selectedStaffName: _msg.Text, messenger: MessengerType, messengerBotId: groupId, clientMessengerId: user.Id.ToString()));
                        else
                        {
                            List<string> msgids = new List<string>() { _msg.Id.ToString() };
                            setStaffResponse = await _apiClient.SetStaffAsync(new BookingSetStaffDto(selectedStaffName: message.Body, messenger: MessengerType, messengerBotId: groupId, clientMessengerId: user.Id.ToString()));
                            await _apiClient.DeleteHistoryAsync(msgids, MessengerType, groupId, user.Id.ToString());
                            await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(visitStep: (int?)VisitCreationStep.GetStaff, messengerMsgId: message.Id.ToString(), stateMetaData: state, messenger: MessengerType, messengerBotId: groupId, clientMessengerId: user.Id.ToString()));
                            GetTime();
                            return;
                        }

                    }
                    catch (Exception)
                    {}
                   
                    times.Items = setStaffResponse.Times;
                }
                if (_msg == null)
                {
                    if (!message.Body.Contains('.'))
                    {
                        setStaffResponse = await _apiClient.SetStaffAsync(new BookingSetStaffDto(selectedStaffName: message.Body, messenger: MessengerType, messengerBotId: groupId, clientMessengerId: user.Id.ToString()));

                        await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(visitStep: (int?)VisitCreationStep.GetStaff, messengerMsgId: message.Id.ToString(), stateMetaData: state, messenger: MessengerType, messengerBotId: groupId, clientMessengerId: user.Id.ToString()));
                        return;
                    }
                    SetTimes();
                    return;
                }
                    
            }
            if (scenario == 1 || scenario == 2)
            {
                state = ST_startBooking;
                try
                {
                    if (message.Body.Contains('.'))
                    {
                        times = await _apiClient.GetScheduleTimesAsync(selectedDate: message.Body,
                        saveHistory: false,
                        messengerMsgId: message.Id.ToString(),
                        stateMetaData: state,
                        messenger: MessengerType,
                        messengerBotId: groupId,
                        clientMessengerId: user.Id.ToString()
                        );
                    }
                    else
                    {

                        var msg = GetMessage(VisitCreationStep.GetDates, last:true, contains: ".");

                        payload.text = JsonConvert.DeserializeObject<ServiceBot.models.payload>(msg.Payload).text; 
                        if (payload.text.Split('@').Length > 1)
                        {
                            times = await _apiClient.GetScheduleTimesAsync(selectedDate: payload.text.Split('@')[1],
                                saveHistory: false,
                                messengerMsgId: message.Id.ToString(),
                                stateMetaData: state,
                                messenger: MessengerType,
                                messengerBotId: groupId,
                                clientMessengerId: user.Id.ToString()
                                );
                        }
                        else
                        {

                            times = await _apiClient.GetScheduleTimesAsync(selectedDate: payload.text.Split('@').Last(),
                                saveHistory: false,
                                messengerMsgId: message.Id.ToString(),
                                stateMetaData: state,
                                messenger: MessengerType,
                                messengerBotId: groupId,
                                clientMessengerId: user.Id.ToString()
                                );
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Error(e, "get time error");
                    throw;
                }
            }
                int placeholder = 0;
            try
            {
                tmp = await GetPayload(message.Id);
            }
            catch (Exception e)
            {
                logger.Info("tmp is null");
                throw;
            }
            if (times.Items.Contains(message.Body) && message.Body!= "-"){

                await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
                    visitStep: (int?)VisitCreationStep.SetTime,
                    messengerMsgId: message.Id.ToString(),
                    stateMetaData: state,
                    messenger: MessengerType,
                    messengerBotId: groupId,
                    clientMessengerId: user.Id.ToString()
                    ));
                tmp.text = message.Body;
                SetTimes(tmp);
                return;
            }
                
                if (!string.IsNullOrWhiteSpace(tmp.text))
                int.TryParse(tmp.text.Split('@').Last(), out placeholder);
                var keyb = KeyboarCreater.createTimeKeyboard(times.Items.ToArray(), times.Items.ToArray(), placeholder, this.state, message.Body, backBTNPayload:"date");
                try
                {
                    msg.sendMessage(user.Id, "Выберите, пожалуйста, время:", keyb);
                    await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
                                 visitStep: (int?)VisitCreationStep.GetDates,
                                 messengerMsgId: message.Id.ToString(),
                                 stateMetaData: state,
                                 messenger: MessengerType,
                                 messengerBotId: groupId,
                            clientMessengerId: user.Id.ToString()
                            ));
                }
                catch (Exception e )
                {
                throw;
            }
         
        }

        private async Task getSelectedServices()
        {
            List<string> _Services = new List<string>();
            List<string> _SelectServices = new List<string>();
            string[] services;
            bool MessageContainsSelected = false;
            services = state.Split('@').Last().Split('|');
            var payload = GetPayload(message.Id).Result.text;
            foreach (var service in services)
            {
                string cat = service;
                if (message.Body == service)
                {
                    if (service.Contains(unselectItem))
                        cat = service.Replace(unselectItem, selectItem);
                    else
                        cat = service.Replace(selectItem, unselectItem);
                }
                if (cat != "")
                    _Services.Add(cat);
                if (cat.Contains(selectItem))
                {
                    MessageContainsSelected = true;
                    _SelectServices.Add(cat);
                }
            }

            logger.Trace($"selected services from payload: {string.Join("|", _SelectServices)} \n message: {message.Body}");
            string mergedServices = "";
            foreach (var c in _Services)
            {
                mergedServices += c + "|";
            }
            if(MessageContainsSelected && message.Body == confirmSelectServiceText)
            {
                ConfirmServices(_SelectServices, VisitCreationStep.SetServices);
                return;
            }
            if (MessageContainsSelected && message.Body != confirmSelectServiceText)
            {
                var keyb = KeyboarCreater.createKeyboardWithPayloadAndFwdBtn(_Services.ToArray(), payload, confirmSelectServiceText, payload);
                string describ = await getDescribtionForService(_SelectServices, VisitCreationStep.SetServices);
                msg.sendMessage(user.Id, SelectServiceText + "\n" + describ, keyb);
            }
            if (!MessageContainsSelected)
            {
                var keyb = KeyboarCreater.createKeyboardWithPayload(_Services.ToArray(), payload, 2, payload);
                msg.sendMessage(user.Id, SelectServiceText, keyb);
            }


            await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
                visitStep: (int?)VisitCreationStep.SetServices,
                messengerMsgId: message.Id.ToString(),
                stateMetaData: ST_startBooking + $"@{mergedServices}",
                messenger: MessengerType,
                messengerBotId: groupId,
                clientMessengerId: user.Id.ToString()
                ));
        }

        /// <summary>
        /// step where categories payload
        /// </summary>
        /// <param name="services"></param>
        /// <param name="step">step where categories payload</param>
        /// <returns></returns>
        private async Task ConfirmServices(List<string> Services, VisitCreationStep step)
        {
            List<string> _SelectedCategory = new List<string>();
            List<string> CategoryTODO = new List<string>();
            var Categories = JsonConvert.DeserializeObject<models.payload>(GetMessage(step, true, contains: $"{selectItem}").Payload).text.Split('|');
            for (int i = 0; i < Services.Count; i++)
            {
                Services[i] = Services[i].Replace(selectItem + " ", "");
            }
            foreach (var category in Categories)
                if (category.Contains(selectItem))
                    _SelectedCategory.Add(category);
            int categotyIndex = 1;
            foreach (var category in _SelectedCategory)
            {
                string _category = "";
                if (category.Contains(selectItem))
                    _category = category.Replace(selectItem + " ", "");
                try
                {
                    if (_category != "")
                    {
                        var setServicesResponse = await _apiClient.SetServicesAsync(new BookingSetServiceDto(
                           selectedCategoryName: _category,
                           selectedServiceNameList: Services,
                           saveHistory: false,
                           messengerMsgId: message.Id.ToString(),
                           stateMetaData: state,
                           messenger: MessengerType,
                           messengerBotId: groupId,
                           clientMessengerId: user.Id.ToString()
                            ));
                        
                        foreach (var serv in setServicesResponse.Items) {
                            CategoryTODO.Add(serv);
                        }
                        
                        break;
                    }
                }
                catch (Exception e)
                {
                    categotyIndex++;
                }
            }
            if (CategoryTODO.Count > 0 && !CategoryTODO.Contains(null))
            {
                GetNextCategory(CategoryTODO.ToArray(),categotyIndex);
                return;
            }
            else
            {
                var scenario = await _apiClient.GetCurrentScenarioAsync(messenger: MessengerType, messengerBotId: groupId, clientMessengerId: user.Id.ToString());
                if (scenario.CurrentScenario == 1)
                {
                    GetDates(scenario: 1);
                    return;
                }
                if (scenario.CurrentScenario == 2 || scenario.CurrentScenario == 3)
                {
                    List<string> staffNames = new List<string>();
                    var staffs = (await _apiClient.GetStaffAsync(messenger: MessengerType, clientMessengerId: user.Id.ToString(), messengerBotId: groupId)).Items;
                    foreach (var staff in staffs)
                        staffNames.Add(staff.Name);

                    await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
                        visitStep: (int?)VisitCreationStep.GetStaff,
                        stateMetaData: state,
                        messengerMsgId: message.Id.ToString(),
                        messenger: MessengerType,
                        messengerBotId: groupId,
                        clientMessengerId: user.Id.ToString()
                        ));
                    var keyb = KeyboarCreater.createKeyboardWithBackBTN(staffNames.ToArray());
                    msg.sendMessage(user.Id, SelectStaffText, keyb);
                }

            }
        }

        private async Task GetDates( BookingSetStaffResultDto setStaffResult = null, BookingSetScenarioResultDto setScenarioResult = null, int? scenario = null)
        {
            VkNet.Model.Keyboard.MessageKeyboard keyb = null;
            if (scenario == 1)
            {
                BookingPagedResultDtoString dates = null;
                try
                {

                    dates = await _apiClient.GetScheduleDatesAsync(
                        saveHistory: false,
                        messengerMsgId: message.Id.ToString(),
                        stateMetaData: state,
                        messenger: MessengerType,
                        messengerBotId: groupId,
                        clientMessengerId: user.Id.ToString()
                    );
                    if(dates.Items.Count > 27)
                    {
                        int count = dates.Items.Count;
                        for (int i = count-1; i > 26; i--)
                        {
                                dates.Items.RemoveAt(i);
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Error(e, "get dates error");
                    var keyboard = KeyboarCreater.createKeyboardMenu();
                    msg.sendMessage(user.Id, "Произошла ошибка, повторите попытку выйдя в меню", keyboard);
                    throw;
                    return;
                }
                 keyb = KeyboarCreater.createKeyboardWithPayloadForEveryButton(dates.Items.ToArray(), dates.Items.ToArray(), 3);
            }
            if(scenario == 3)
            {
                keyb = KeyboarCreater.createKeyboardWithPayloadForEveryButton(setScenarioResult.Dates.ToArray(), setScenarioResult.Dates.ToArray(), 4);
            }
            if(scenario == 2)
                keyb = KeyboarCreater.createKeyboardWithPayloadForEveryButton(setStaffResult.Dates.ToArray(), setStaffResult.Dates.ToArray(),3);
            msg.sendMessage(user.Id, SelectDateText, keyb);
            await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
                visitStep: (int?)VisitCreationStep.GetDates,
                messengerMsgId: message.Id.ToString(),
                stateMetaData: state,
                messenger: MessengerType,
                messengerBotId: groupId,
                clientMessengerId: user.Id.ToString()
                ));
        }

        private async Task GetNextCategory(string[] serviceTODO, int categoryIndex)
        {
            List<string> items = new List<string>();
            try
            {
                string mergedItems = "";

                foreach (var item in serviceTODO)
                {
                    items.Add(unselectItem + " " + item);
                    mergedItems += unselectItem + " " + item + "|";
                }
                var keyb = KeyboarCreater.createKeyboardWithPayload(items.ToArray(),categoryIndex.ToString(),2, categoryIndex.ToString());
                await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
                visitStep: (int?)VisitCreationStep.SetServices,
                messengerMsgId: message.Id.ToString()+ "_prev",
                stateMetaData:  state,
                messenger: MessengerType,
                messengerBotId: groupId,
                clientMessengerId: user.Id.ToString()
                ));
                await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
                visitStep: (int?)VisitCreationStep.SetServices,
                messengerMsgId: message.Id.ToString(),
                stateMetaData: ST_startBooking + $"@{mergedItems}",
                messenger: MessengerType,
                messengerBotId: groupId,
                clientMessengerId: user.Id.ToString()
                ));
                msg.sendMessage(user.Id, SelectServiceText, keyb);
            }
            catch (Exception e)
            {
                logger.Error(e, "sendmessage error");
                throw;
            }
        }

        /// <summary>
        /// step where categories payload
        /// </summary>
        /// <param name="services"></param>
        /// <param name="step">step where categories payload</param>
        /// <returns></returns>
        private async Task<string> getDescribtionForService(List<string> services, VisitCreationStep step)
        {
            string _return = "";
            List<string> _SelectedCategory = new List<string>();
            var Categories = JsonConvert.DeserializeObject<models.payload>(GetMessage(step, true, contains: $"{selectItem}").Payload).text.Split('|');
            var companyInfo = await _apiClient.GetClientPreferredCompanyInfoAsync(messenger: MessengerType, clientMessengerId: user.Id.ToString(), messengerBotId: groupId);
            for (int i = 0; i < services.Count; i++)
            {
                services[i] = services[i].Replace(selectItem + " ", "");
            }
            foreach(var category in Categories)
            {
                if (category.Contains(selectItem))
                    _SelectedCategory.Add(category.Replace(selectItem + " ", ""));
            }
            foreach (var category in _SelectedCategory) 
            {
                BookingCatPagedResultDtoServiceOutputDto result = null;
                try
                {
                    result = await _apiClient.GetServicesAsync(category, services, messenger: MessengerType, clientMessengerId: user.Id.ToString(), messengerBotId: groupId);
                    if (result.Items.Count < 1)
                        continue;
                }
                catch (Exception e)
                {
                    logger.Warn(e, "GetServiceDescription error");
                    continue;
                }
                var apiProvider = companyInfo.ApiProvider;
                var scenario = result.CurrentScenario.Value;
                if(scenario == 1)
                   _return += category + "\n";
                if(scenario == 2 || scenario == 3)
                    _return += "цены на виды услуг" + "\n";
                   foreach (var item in result.Items)
                   {
                      
                       string description ="";
                                
                       var priceString = item.PriceMax == item.PriceMin ? $"{item.PriceMin} {botSettings.Currency}." : $"{item.PriceMin}-{item.PriceMax} {botSettings.Currency}.";
                       _return += $"- {item.FullName} ({priceString}) {description} \n";
                   }

                   var totalString = result.Items.Sum(x => x.PriceMax) == result.Items.Sum(x => x.PriceMin) ? $"{result.Items.Sum(x => x.PriceMax)} {botSettings.Currency}." : $"{result.Items.Sum(x => x.PriceMin)}-{result.Items.Sum(x => x.PriceMin)} {botSettings.Currency}.";
                   _return += $"Итого:  {totalString}\n";
                

                
            }



            return _return;
        }
        private static string GetHyperLink(string imageUrl, string text)
        {
            return !string.IsNullOrWhiteSpace(imageUrl) ? $"<u><i><a href='{imageUrl}'>{text}</a></i></u>" : string.Empty;
        }
        private async Task getSelectedCategory()
        {
            var history = await getDefinedHistory(VisitCreationStep.GetCategories);
            mId id;
            List<ulong> MessageList = new List<ulong> { ulong.Parse(history.MessageId) };
            List<string> FieldList = new List<string> { "" };
            List<string> _Categories = new List<string>();
            id.MessageId = MessageList;
            id.fields = FieldList;
            string[] categories;
            models.payload payload = null;
            if (message.Payload == null)
            {
                payload = await GetPayload(message.Id);
            }
            else
            {
                payload = JsonConvert.DeserializeObject<ServiceBot.models.payload>(message.Payload);
            }

            categories = payload.text.Split('|');
            bool MessageContainsSelected = false;
                foreach(var category in categories)
                {
                    string cat = category;
                    if(message.Body == category)
                    {
                        if (category.Contains(unselectItem))
                            cat = category.Replace(unselectItem, selectItem);
                        else
                            cat = category.Replace(selectItem, unselectItem);
                    }
                    if (cat != "")
                        _Categories.Add(cat);
                    if (cat.Contains(selectItem))
                        MessageContainsSelected = true;
                }
            
            string mergedCetegories = "";
            foreach(var c in _Categories)
            {
                mergedCetegories += c + "|";
            }
            if (MessageContainsSelected && message.Body != confirmSelectCategoryText)
            {
                var keyb = KeyboarCreater.createKeyboardWithPayloadAndFwdBtn(_Categories.ToArray(), mergedCetegories, confirmSelectCategoryText);
                msg.sendMessage(user.Id, SelectCategoryText, keyb);
            }
            if (!MessageContainsSelected)
            {
                var keyb = KeyboarCreater.createKeyboardWithPayload(_Categories.ToArray(), mergedCetegories, 2);
                msg.sendMessage(user.Id, SelectCategoryText, keyb);
            }
            
            if (message.Body == confirmSelectCategoryText)
            {
                ConfirmCategories(_Categories.ToArray());
            }
            
        }

        private async Task ConfirmCategories(string[] categories)
        {
            List<string> SelectedCategories = new List<string>();
            List<string> items = new List<string>();

            string mergedItems = "";
            foreach (var category in categories)
            {
                if (category.Contains(selectItem))
                    SelectedCategories.Add(category.Replace(selectItem + " ", ""));
            }
            try
            {
                var categoryResponse = await _apiClient.SetCategoriesAsync(new BookingSetCategoryDto(
                    messenger: MessengerType,
                    selectedServiceCategoryNameList: SelectedCategories.ToArray(),
                    saveHistory: false,
                    messengerMsgId: message.Id.ToString(),
                    stateMetaData: state,
                    messengerBotId: groupId,
                    clientMessengerId: user.Id.ToString()
                    ));

                foreach (var item in categoryResponse.Items)
                {
                    items.Add(unselectItem + " " + item);
                    mergedItems += unselectItem + " " + item + "|";
                }

                await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
                    visitStep: (int?)VisitCreationStep.SetServices,
                    messengerMsgId: message.Id.ToString(),
                    stateMetaData: state + $"@{mergedItems}",
                    messenger: MessengerType,
                    messengerBotId: groupId,
                    clientMessengerId: user.Id.ToString()
                    ));
                var keyb = KeyboarCreater.createKeyboardWithPayload(items.ToArray(),"0", 2,"0");
                
                msg.sendMessage(user.Id, SelectServiceText, keyb);
            }
            catch (Exception e )
            {
                logger.Error(e,"set category error trace:" + mergedItems);
                throw;
            }
        }

        private async Task<models.payload> GetPayload(long? Id)
        {
            mId id;
            List<ulong> MessageList = new List<ulong> { ulong.Parse(Id.ToString()) };
            List<string> FieldList = new List<string> { "" };
            id.MessageId = MessageList;
            id.fields = FieldList;
            var _message = await _api.Messages.GetByIdAsync(id.MessageId, id.fields, 0, false, ulong.Parse(groupId));
            if (_message[0].Payload != string.Empty)
                return JsonConvert.DeserializeObject<ServiceBot.models.payload>(_message[0].Payload);
            else return null;
        }
        
        private async Task<MessengerHistoryDto> getDefinedHistory(VisitCreationStep step,bool lastHistory = false, string messageId = null)
        {
            var history = await _apiClient.GetHistoryAsync(messenger: MessengerType, messengerBotId: groupId, clientMessengerId: user.Id.ToString());
            if (history.MessengerHistory.Count <= 5)
                lastHistory = false;
            MessengerHistoryDto last = null;
            foreach (var h in history.MessengerHistory)
            {
                if(!lastHistory)
                    if (h.Step == (int?)step)
                        return h;
                if (h.Step == (int?)step)
                {
                    if(messageId == null)
                                    last = h;
                    else
                    {
                        if (h.MessageId == messageId)
                                                last = h;
                    }
                }
            }
            return last;
        }
        private async Task setStaffBooking()
        {
            try
            {

                var _scenario = await _apiClient.GetCurrentScenarioAsync(
                    messenger: MessengerType,
                    messengerBotId: groupId,
                    clientMessengerId: user.Id.ToString()
                    );
               
                
                    var setStaffRespone = await _apiClient.SetStaffAsync(new BookingSetStaffDto(
                        selectedStaffName: message.Body,
                        saveHistory: false,
                        messengerMsgId: message.Id.ToString(),
                        stateMetaData: state,
                        messenger: MessengerType,
                        messengerBotId: groupId,
                        clientMessengerId: user.Id.ToString()
                        ));
                    if (_scenario.CurrentScenario == 1)
                    {
                        getCategoryList(setStaffRespone: setStaffRespone, _sendMessege: true, scenario: 1);
                    return;
                    }
                    if (_scenario.CurrentScenario == 2)
                    {
                        GetDates(setStaffRespone,scenario: 2);
                    return;
                    }

                    if(_scenario.CurrentScenario == 3)
                    {
                    GetTime();
                    return;
                    }
                
            }
            
            catch (Exception e)
            {
                logger.Error(e, "setstaff error");
                throw;
            }
        }

        private async Task<string[]> getCategoryList(BookingSetStaffResultDto setStaffRespone = null, BookingSetScenarioResultDto setScenarioResultResponse = null, bool _sendMessege = false, int scenario = 0)
        {
            string payload = "";
            List<string> Category = new List<string>();
            if (scenario == 1)
            {
                foreach (var cat in setStaffRespone.ServiceCategories)
                {
                    Category.Add(unselectItem + " " + cat);
                }
                if (_sendMessege)
                {
                    foreach (var c in Category)
                    {
                        payload += c + "|";
                    }
                    var keyb = KeyboarCreater.createKeyboardWithPayload(Category.ToArray<string>(), payload,2);
                    msg.sendMessage(user.Id, SelectCategoryText, keyb);
                    await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
                        visitStep: (int?)VisitCreationStep.GetCategories,
                        messengerMsgId: message.Id.ToString(),
                        stateMetaData: state,
                        messenger: MessengerType,
                        messengerBotId: groupId,
                        clientMessengerId: user.Id.ToString()
                        ));
                }
            }
            else if(scenario == 2)
            {
                var categories = setScenarioResultResponse.ServiceCategories;
                
                if (_sendMessege)
                {
                    foreach (var cat in categories)
                    {
                        Category.Add(unselectItem + " "+ cat);
                        payload += unselectItem + " " + cat + "|";
                    }
                    var keyb = KeyboarCreater.createKeyboardWithPayload(Category.ToArray<string>(), payload, 2);
                    msg.sendMessage(user.Id, SelectCategoryText, keyb);
                    await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
                        visitStep: (int?)VisitCreationStep.GetCategories,
                        messengerMsgId: message.Id.ToString(),
                        stateMetaData: state,
                        messenger: MessengerType,
                        messengerBotId: groupId,
                        clientMessengerId: user.Id.ToString()
                        ));
                }
            }
            else if(scenario == 3)
            {
                var _payload = await GetPayload(message.Id);
                var categories = await _apiClient.GetCategoriesAsync(selectedDate: _payload.text, messenger: MessengerType, clientMessengerId: user.Id.ToString(), messengerBotId: groupId);

                if (_sendMessege)
                {
                    foreach (var cat in categories.Items)
                    {
                        Category.Add(unselectItem + " " + cat);
                        payload += unselectItem + " " + cat + "|";
                    }
                    var keyb = KeyboarCreater.createKeyboardWithPayload(Category.ToArray<string>(), payload,2);
                    msg.sendMessage(user.Id, SelectCategoryText, keyb);
                    await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
                        visitStep: (int?)VisitCreationStep.GetCategories,
                        messengerMsgId: message.Id.ToString(),
                        stateMetaData: state,
                        messenger: MessengerType,
                        messengerBotId: groupId,
                        clientMessengerId: user.Id.ToString()
                        ));
                }
            }
            return Category.ToArray<string>();
        }

        private async Task setScenarioBooking(BookingStartResultDto startInfo)
        {
            bool messegeContainsScenario = startInfo.ScenarioList.Contains(message.Body);
            if (messegeContainsScenario)
            {
                var _scenario = ScenarioByName[message.Body];
                try
                {

                    var response = await _apiClient.SetScenarioAsync(new BookingSetScenarioDto(
                        scenario: _scenario,
                        saveHistory: false,
                        messengerMsgId: message.Id.ToString(),
                        stateMetaData: state,
                        messenger: MessengerType,
                        messengerBotId: groupId,
                        clientMessengerId: user.Id.ToString()
                        ));

                    if (_scenario == 1)
                        getStaff(response, true);
                    if (_scenario == 2)
                        getCategoryList( setScenarioResultResponse: response,_sendMessege: true, scenario: 2);
                    if (_scenario == 3)
                        GetDates(setScenarioResult: response,scenario: 3);
                }
                catch (Exception e )
                {
                    logger.Error(e, "set scenario error");
                    throw;
                }
            }
            else
            {
                if (message.Body != "Начать")
                    msg.sendMessage(user.Id, IncorrectDataText);
                getScenarioBooking(startInfo);

            }

        }

        private async Task<string[]> getStaff(BookingSetScenarioResultDto response, bool sendMessege)
        {
            List<string> staffNames = new List<string>();
           foreach(var staff in response.Staff)
            {
                staffNames.Add(staff.Name);
            }
            var keyb = KeyboarCreater.createKeyboardWithBackBTN(staffNames.ToArray<string>());
            if(sendMessege)
                msg.sendMessage(user.Id, SelectStaffText, keyb);
            try
            {

                await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
                    visitStep: (int?)VisitCreationStep.GetStaff,
                    messengerMsgId: message.Id.ToString(),
                    stateMetaData: state,
                    messenger: MessengerType,
                    messengerBotId: groupId,
                    clientMessengerId: user.Id.ToString()
                    ));
            }
            catch (Exception e)
            {
                logger.Error(e, "sethistory error");
                throw;
            }
            return staffNames.ToArray<string>();
        }


        private async Task getScenarioBooking(BookingStartResultDto startInfo)
        {
            var keyb = KeyboarCreater.createKeyboardWithMenuBTN(startInfo.ScenarioList.ToArray<string>());
            msg.sendMessage(user.Id, SelectScenarioText, keyb);
            try
            {
                await _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
               visitStep: (int?)VisitCreationStep.SetScenario,
               messengerMsgId: message.Id.ToString(),
               stateMetaData: ST_startBooking,
               messenger: MessengerType,
               messengerBotId: groupId,
               clientMessengerId: user.Id.ToString()
               ));
            }
            catch (Exception e)
            {
                logger.Error(e, "sethistory error");
                throw;
            }

        }

        private async Task getPricelist()
        {
            try
            {
                var companyInfo = await _apiClient.GetClientPreferredCompanyInfoAsync(MessengerType, groupId, user.Id.ToString());
                if(companyInfo.ImagePriceUrl == null)
                    msg.sendMessage(user.Id, "На данный момент нет прайс-листа");
                else
                     msg.sendMessage(user.Id, PriceText, ImageUrl: companyInfo.ImagePriceUrl);
            }
            catch (Exception e)
            {
                logger.Error(e, "can't get client company");
                throw;
            }
        }

        private async Task CompanySelect()
        {
            try
            {
                    await _apiClient.ResetPreferredCompanySetCompanyAsync(new ResetCompanyDto(
                        selectedCompanyName: message.Body,
                        messenger: MessengerType,
                        messengerBotId: groupId, 
                        clientMessengerId: user.Id.ToString()
                        ));    
                    var keyb = KeyboarCreater.createStartKeyboard();
                msg.sendMessage(user.Id, SelectScenarioText, keyb);
                    _apiClient.SetHistoryForClientAsync(new ClientAddHistoryDto(
                        messengerMsgId: message.Id.ToString(),
                        stateMetaData: ST_menu,
                        messenger: MessengerType, 
                        messengerBotId: groupId, 
                        clientMessengerId: user.Id.ToString()
                        )); 
                _apiClient.SetHistoryAsync(new BookingAddHistoryDto(
                        messengerMsgId: message.Id.ToString(),
                        stateMetaData: ST_menu,
                        messenger: MessengerType,
                        messengerBotId: groupId,
                        clientMessengerId: user.Id.ToString()
                        ));
            }
            catch (Exception e)
            {
                logger.Error(e, "ResetPreferredCompany error");
                throw;
            }

        }

        
        
        
    }
}
