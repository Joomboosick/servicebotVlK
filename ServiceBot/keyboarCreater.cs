﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceBot
{
    class KeyboarCreater
    {
        VkNet.Model.Keyboard.KeyboardBuilder keyb = null;
        public KeyboarCreater(VkNet.Model.Keyboard.KeyboardBuilder keyboard)
        {
            keyb = keyboard;
        }
        const string startBookingBTN = "Записаться";
        const string cancelBookingBTN = "Отменить запись";
        const string pricelistBTN = "Прайс-лист";
        const string changeBranchBTN = "Сменить филиал";
        const string CallUsBTN = "Позвонить нам";
        //------------------------------------------------------------------------------------------------------
        public VkNet.Model.Keyboard.MessageKeyboard createKeyboardMenu()
        {
            keyb.Clear();
            keyb.AddButton("меню", "fksdf");
            var readyKeyboard = keyb.Build();
            return readyKeyboard;
        }
        //------------------------------------------------------------------------------------------------------
        public VkNet.Model.Keyboard.MessageKeyboard createNullKeyboard()
        {
            keyb.Clear();
            var keyboard = new VkNet.Model.Keyboard.KeyboardBuilder("text");
            keyboard.AddButton("","text123123");
            var readyKeyboard = keyboard.Build();
            return readyKeyboard;
        }
        //------------------------------------------------------------------------------------------------------
        public VkNet.Model.Keyboard.MessageKeyboard createCompaniesKeyboard(string[] text, int placeholder, string state, string city)
        {
            keyb.Clear();

            int l = 0;
            string[][] result = text.GroupBy(s => l++ / 8).Select(g => g.ToArray()).ToArray();
            var currentKeyboard = result[placeholder];

            for (int i = 0; i < currentKeyboard.Length; i++)
            {
                if (i % 2 == 0 && i != 0 && i != result[placeholder].Length - 1)
                {
                    keyb.AddLine();
                }
                keyb.AddButton(currentKeyboard[i], state+"@"+city); // text123123123 is required parameter, but in long poll it doesn't work
            }
            bool right = false;
            bool left = false;
            try
            {
                if (result[placeholder - 1] != null)
                    left = true;
            } catch (Exception e)
            {
                left = false;
            }

            try
            {
                if (result[placeholder + 1] != null)
                    right = true;
            } catch (Exception e)
            {
                right = false;
            }


            keyb.AddLine();
            if (left) keyb.AddButton("⬅️", state + "@" +city+ "@" + (placeholder - 1).ToString());
            if (right) keyb.AddButton("➡️", state + "@" +city+ "@" + (placeholder + 1).ToString());
            var readyKeyboard = keyb.Build();
            return readyKeyboard;
        }
        //------------------------------------------------------------------------------------------------------
        public VkNet.Model.Keyboard.MessageKeyboard createStartKeyboard(bool flag = true)
        {
            keyb.Clear();
            keyb.AddButton(startBookingBTN, "text1231312"); // text123123123 is required parameter, but in long poll it doesn't work
            keyb.AddButton(cancelBookingBTN, "text1231312");
            keyb.AddLine();
            keyb.AddButton(pricelistBTN, "text1231312");
            if(flag)
                keyb.AddButton(changeBranchBTN, "text1231312");
            else
                keyb.AddButton(CallUsBTN, "text1231312");
            var readyKeyboard = keyb.Build();
            return readyKeyboard;
        }
        //------------------------------------------------------------------------------------------------------
        public VkNet.Model.Keyboard.MessageKeyboard createKeyboardWithBackBTN(string[] text)
        {
                keyb.Clear();
                for (int i = 0; i < text.Length; i++)
                {
                    if (i % 3 == 0 && i != 0 && i != text.Length)
                        keyb.AddLine();
                    keyb.AddButton(text[i], "text123123123"); // text123123123 is required parameter, but in long poll it doesn't work
                }
                keyb.AddLine();
                keyb.AddButton("⬅️ Назад", "fksdf");
                var readyKeyboard = keyb.Build();
                return readyKeyboard;
            
        }
        public VkNet.Model.Keyboard.MessageKeyboard createKeyboardWithPayload(string[] text, string payload, int countLines = 3, string backBtnPayload = "fksdf")
        {
            keyb.Clear();
            for (int i = 0; i < text.Length; i++)
            {
                if (i % countLines == 0 && i != 0 && i != text.Length)
                    keyb.AddLine();
                keyb.AddButton(text[i], payload);
            }
            keyb.AddLine();
            keyb.AddButton("⬅️ Назад", backBtnPayload);
            var readyKeyboard = keyb.Build();
            return readyKeyboard;

        }

        public VkNet.Model.Keyboard.MessageKeyboard createKeyboardWithPayloadForEveryButton(string[] text, string[] payload, int countLines, string backBtnText = "⬅️ Назад")
        {
            keyb.Clear();


            for (int i = 0; i < text.Length; i++)
            {
                
                if (i % countLines == 0 && i != 0)
                    keyb.AddLine();
                keyb.AddButton(text[i], payload[i]);
            }
            keyb.AddLine();


            keyb.AddButton(backBtnText, "fksdf");
            var readyKeyboard = keyb.Build();
            return readyKeyboard;

        }

        public VkNet.Model.Keyboard.MessageKeyboard createTimeKeyboard(string[] text, string[] payload, int placeholder, string state = "", string date = "", string backBTNPayload = "fksdf")
        {
            keyb.Clear();

            int l = 0;
            string[][] result = text.GroupBy(s => l++ / 32).Select(g => g.ToArray()).ToArray();
            var currentKeyboard = result[placeholder];

            for (int i = 0; i < currentKeyboard.Length; i++)
            {
                
                if (i % 4 == 0 && i != 0 && i!=currentKeyboard.Length)
                    keyb.AddLine();
                keyb.AddButton(currentKeyboard[i], payload[i]);
            }
            keyb.AddLine();

            bool right = false;
            bool left = false;
            try
            {
                if (result[placeholder - 1] != null)
                    left = true;
            }
            catch (Exception e)
            {
                left = false;
            }

            try
            {
                if (result[placeholder + 1] != null)
                    right = true;
            }
            catch (Exception e)
            {
                right = false;
            }


            if (left) keyb.AddButton("⬅️", state + "@" + date + "@" + (placeholder - 1).ToString());
            if (right) keyb.AddButton("➡️", state + "@" + date + "@" + (placeholder + 1).ToString());
            if(left || right)
                keyb.AddLine();

            keyb.AddButton("⬅️ Назад", backBTNPayload);
            var readyKeyboard = keyb.Build();
            return readyKeyboard;

        }
        public VkNet.Model.Keyboard.MessageKeyboard createKeyboardCompanies(string[] text, string payload, int startIndex = 0)
        {
            payload = payload.Split('@').Last();
            var _payload = payload.Split('|');
            keyb.Clear();
            if(_payload.Length> 10)
            {
                for (int i = 0; i < text.Length; i++)
                {
                    if (i < startIndex)
                        continue;
                    if (i+startIndex > 10)
                        break;
                    var temp = i + 1;
                    if (temp % 2 == 0 && i != 0 && i != text.Length)
                        keyb.AddLine();
                    keyb.AddButton(text[i], "changeBranch");

                }
            }
            else
                for (int i = 0; i < text.Length; i++)
                {
                    var temp = i + 1;
                    if (temp % 2 == 0 && i != 0 && i != text.Length)
                        keyb.AddLine();
                    keyb.AddButton(text[i], "changeBranch");
                }
            var readyKeyboard = keyb.Build();
            return readyKeyboard;

        }


        public VkNet.Model.Keyboard.MessageKeyboard createKeyboardWithPayloadAndFwdBtn(string[] text, string payload, string fwdBtnText, string backBtnPayload = "fksdf")
        {
            keyb.Clear();
            for (int i = 0; i < text.Length; i++)
            {
                if (i % 2 == 0 && i != 0 && i != text.Length)
                    keyb.AddLine();
                keyb.AddButton(text[i], payload);
            }
            keyb.AddLine();
            keyb.AddButton("⬅️ Назад", "fksdf"); keyb.AddButton(fwdBtnText, payload);
            var readyKeyboard = keyb.Build();
            return readyKeyboard;

        }
        public VkNet.Model.Keyboard.MessageKeyboard createKeyboardWithMenuBTN(string[] text)
        {
            keyb.Clear();
            if(text != null)
                for (int i = 0; i < text.Length; i++)
                {
                    if (i % 4 == 0 && i != 0 && i != text.Length - 1)
                        keyb.AddLine();
                    keyb.AddButton(text[i], "text123123123"); // text123123123 is required parameter, but in long poll it doesn't work
                }
            keyb.AddLine();
            keyb.AddButton("меню", "fksdf");
            var readyKeyboard = keyb.Build();
            return readyKeyboard;

        }
        //------------------------------------------------------------------------------------------------------
    }
}
