﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceBot
{
    class StateService
    {

        const string startBookingBTN = "Записаться";
        const string cancelBookingBTN = "Отменить запись";
        const string pricelistBTN = "Прайс-лист";
        const string changeBranchBTN = "Сменить филиал";
        const string CallUsBTN = "Позвонить нам";

        const string ST_menu = "menu";
        const string ST_startBooking = "startBooking";
        const string ST_cancelBooking = "cancelBooking";
        const string ST_pricelist = "pricelist";
        const string ST_changeBranch = "changeBranch";
        const string ST_start = "start";
        const string ST_prevStep = "prevStep";

        public string GetNextState(string text = "", string currentState = "")
        {

            string state = "";

            if (currentState.Length > 0)
            {
                if (currentState.Contains(ST_startBooking))
                    state = currentState;
                else
                    switch (currentState)
                    {
                        case ST_start:
                            state = currentState;
                            break;
                        case "CompanySelect":
                            state = currentState;
                            break;
                        case ST_startBooking:
                            state = currentState;
                            break;
                        default:
                            break;
                    }
            }

            switch (text)
            {
                case "⬅️ Назад":
                    state = ST_prevStep;
                    break;
                case "Меню":
                    state = ST_menu;
                    break;

                case "меню":
                    state = ST_menu;
                    break;
                case "Вернуться в меню":
                    state = ST_menu;
                    break;
                case "Начать":
                    state = ST_changeBranch;
                    break;
                case startBookingBTN:
                    state = ST_startBooking;
                    break;
                case cancelBookingBTN:
                    state = ST_cancelBooking;
                    break;
                case pricelistBTN:
                    state = ST_pricelist;
                    break;
                case changeBranchBTN:

                    state = ST_changeBranch;
                    break;
                case CallUsBTN:
                    state = "call";
                    break;
                default:
                    if (text =="start" || text == "/start" || text == "привет" || text == "Start" || text == "/Start" || text == "привет")
                        state = ST_startBooking;
                    break;
                }
            switch (text.ToLower())
            {
                case "start":
                    state = ST_startBooking;
                    break;
                case "/start":
                    state = ST_startBooking;
                    break;
                case "привет":
                    state = ST_startBooking;
                    break;


            }
            if (state.Length > 0)
                        return state;
            else 
                return null;
        }
    }
}
