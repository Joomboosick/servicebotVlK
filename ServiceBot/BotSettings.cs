﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace ServiceBot
{
    class users
    {
        public string Token { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string TenacyName { get; set; }
        public string Currency { get; set; }
        public string BaseUrl { get; set; }
        public ulong GroupId { get; set; }
    }
    class BotSettings
    {
        public users[] users { get; set; }

        public BotSettings()
        {

        }

        static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public static BotSettings getSettings()
        {
            string json = "";
            try
            {
                json = File.ReadAllText("settings.json");
            }
            catch (Exception e)
            {
                logger.Error(e, "can't read settings file");
            }
            //deserialize setting from input file "settings.json"
            //file should be in  the same directory with .exe file
            var BotSettings = JsonConvert.DeserializeObject<BotSettings>(json);
            return BotSettings;
        }

    }
}
