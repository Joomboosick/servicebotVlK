﻿using ServiceBotApi;
using ServiceBotApi.Models;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Enums.SafetyEnums;
using VkNet.Model;
using VkNet.Model.RequestParams;
using Newtonsoft.Json;
using NLog;
using System.Threading;

namespace ServiceBot
{
    class Program
    {
        static VkApi api;
        static ServiceBotAPI _apiClient;
        static LongPollServerResponse server;
        static ulong groupid;
        static users botS;

        static void Main(string[] args)
        {
            //settings init

            var logger = LogManager.GetCurrentClassLogger();

            var settings = BotSettings.getSettings();
            foreach (var setting in settings.users)
            {
                try
                {
                    botS = setting;
                    //rpv api init
                    api = new VkApi();
                    _apiClient = new ServiceBotAPI(new System.Net.Http.HttpClient(), true);
                    _apiClient.BaseUri = new Uri(setting.BaseUrl);
                    //rpv api auth
                    var authResult = _apiClient.Authenticate(new AuthenticateModel(setting.UserName, setting.Password, setting.TenacyName));
                    _apiClient.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authResult.AccessToken);
                    api.Authorize(new ApiAuthParams()
                    { AccessToken = setting.Token }
                    );


                    //getting long poll server from vk
                    var s = api.Groups.GetLongPollServer(setting.GroupId);
                    server = s;
                    groupid = botS.GroupId;
                    //boot
                    new Thread(() => boot()).Start();
                    logger.Info($"starting bot {setting.TenacyName}");
                }
                catch (Exception ex)
                {
                    // NLog: catch any exception and log it.
                    logger.Error(ex, "Stopped program because of exception");
                    throw;
                }
            }
            
        }
        
        private static void boot()
        {
            //base classes init
            var settings = new Dictionary<string, object>();
            var logger = LogManager.GetCurrentClassLogger();
            settings.Add("api", api);
            settings.Add("server", server);
            settings.Add("groupid", groupid);
            settings.Add("apiClient", _apiClient);
            settings.Add("botSettings", botS);
            var listener = new Listener(settings);

            logger.Info("Bot has been started");//let people know

            //some magic here
            
            listener.listen();
        }

        
    }
}
