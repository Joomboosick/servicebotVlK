﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ServiceBot.models
{
    class payload
    {
        [JsonProperty("text")]
        public string text { get; set; }

        [JsonProperty("command")]
        public string command { get; set; }
    }
}
