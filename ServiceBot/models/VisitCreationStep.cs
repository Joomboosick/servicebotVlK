﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceBot.models
{
    public enum VisitCreationStep
    {
        Undefined = 0,

        Start = 10,

        GetCompanies = 20,

        SetCompany = 30,

        SetScenario = 40,

        GetStaff = 50,

        SetStaff = 60,

        GetCategories = 70,

        SetCategories = 80,

        SetServices = 90,

        GetDates = 100,

        GetTimes = 110,

        SetTime = 120,

        Finalize = 130
    }
}
