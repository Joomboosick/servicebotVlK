﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Model.RequestParams;
using VkNet.Model.Keyboard;
using Newtonsoft.Json;
using System.Net;
using Newtonsoft.Json.Linq;
using NLog;

namespace ServiceBot
{
    class MessageWrapper
    {

        private VkApi api;

        private Random rand;

        IEnumerable<VkNet.Model.Attachments.MediaAttachment> attachments;
        string groupId;

        Logger logger = LogManager.GetCurrentClassLogger();
        public MessageWrapper (VkApi api, string groupId)
        {
            this.api = api;
            this.rand = new Random();
            this.groupId = groupId;
        }

        private struct mId
        {
            public IEnumerable<ulong> MessageId;
            public IEnumerable<string> fields;
        }
        public models.payload GetPayload(long? Id)
        {
            mId id;
            List<ulong> MessageList = new List<ulong> { ulong.Parse(Id.ToString()) };
            List<string> FieldList = new List<string> { "" };
            id.MessageId = MessageList;
            id.fields = FieldList;
            var _message =  api.Messages.GetById(id.MessageId, id.fields, 0, false, ulong.Parse(groupId));
            if (_message[0].Payload != null)
                return JsonConvert.DeserializeObject<ServiceBot.models.payload>(_message[0].Payload);
            else return new models.payload() { text = _message[0].Text };
        }
        public void sendMessage(long userId, string message, MessageKeyboard readyKb = null, string ImageUrl = null, bool logging  = true)
        {
            if (ImageUrl != null)
            {
                var localUrl = DateTime.Now.Millisecond.ToString()+".jpg";
                WebClient webClient = new WebClient();
                webClient.DownloadFile(ImageUrl, localUrl);
                var uploadServer = this.api.Photo.GetMessagesUploadServer(userId);
                var wc = new WebClient();
                var responseImg = Encoding.ASCII.GetString(wc.UploadFile(uploadServer.UploadUrl, localUrl));
                JToken o = JToken.Parse(responseImg);
                attachments = api.Photo.SaveMessagesPhoto(responseImg);
            }

            try
            {
                this.api.Messages.Send(new MessagesSendParams()
                {
                    RandomId = DateTime.UtcNow.Millisecond + rand.Next(0, 99999999),//just don't touch. It's neccessary
                    UserId = userId,
                    Message = message,
                    Keyboard = readyKb,
                    Attachments = attachments
                });
                if(logging)
                logger.Info("Response: "+message);
            }
            catch (Exception e)
            {
                logger.Error(e, "can't send message");
                logger.Trace($"userid: {userId}\n" +
                    $"message: {message}\n");
                throw;
            }
        }

    }
}
